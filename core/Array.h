#ifndef ARRAY_H
#define ARRAY_H

template<class T>
class Array{
public:
    Array(T data);
    T data;
};

#include "Array.cpp"
#endif