#ifndef KIWICORE_H
#define KIWICORE_H

#include <unordered_map>
#include "CoreFunctions.h"

typedef std::string (*functionPointer)(std::string);

std::unordered_map<std::string, functionPointer> reservedWords = {
        {"print",printBuilder},
        {"for",forBuilder},
        {"if",ifBuilder},
        {"while",whileBuilder},
};

#endif