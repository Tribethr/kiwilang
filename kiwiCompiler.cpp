#include <iostream>
#include <vector>
#include <fstream>
#include "core/Array.h"
#include "parsing/stringParsing.h"

std::string interpret(std::vector<std::string> data){
    return "";
}

int main(){
    std::string line;
    std::ifstream mainFile ("KiwiCode/input/main.kiwi");
    if (mainFile.is_open()){
        while ( getline (mainFile,line) ){
            std::cout << split(&line)[0] << '\n';
        }
        mainFile.close();
    }else{
         std::cerr << "Unable to open file";
    }
    return 0;
}